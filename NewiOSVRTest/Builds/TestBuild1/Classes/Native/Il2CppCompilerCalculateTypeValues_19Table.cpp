﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GvrAudio2627885619.h"
#include "AssemblyU2DCSharp_GvrAudio_Quality2125366261.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerData1929858338.h"
#include "AssemblyU2DCSharp_GvrAudio_SpatializerType3348390394.h"
#include "AssemblyU2DCSharp_GvrAudio_RoomProperties2834448096.h"
#include "AssemblyU2DCSharp_GvrAudioListener1521766837.h"
#include "AssemblyU2DCSharp_GvrAudioRoom1253442178.h"
#include "AssemblyU2DCSharp_GvrAudioRoom_SurfaceMaterial3590751945.h"
#include "AssemblyU2DCSharp_GvrAudioSoundfield1301118448.h"
#include "AssemblyU2DCSharp_GvrAudioSource2307460312.h"
#include "AssemblyU2DCSharp_GvrArmModel1664224602.h"
#include "AssemblyU2DCSharp_GvrArmModelOffsets2241056642.h"
#include "AssemblyU2DCSharp_GvrController1602869021.h"
#include "AssemblyU2DCSharp_GvrControllerVisual3328916665.h"
#include "AssemblyU2DCSharp_GvrControllerVisualManager1857939020.h"
#include "AssemblyU2DCSharp_GvrPointerManager2205699129.h"
#include "AssemblyU2DCSharp_GvrRecenterOnlyController2745329441.h"
#include "AssemblyU2DCSharp_GvrTooltip801170144.h"
#include "AssemblyU2DCSharp_Gvr_Internal_AndroidNativeContro1389606029.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorClientSocke2001911543.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig616150261.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorConfig_Mode1624619217.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorGyroEvent1858389926.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorAccelEvent621139879.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent1122923020.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_A936529327.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorTouchEvent_3000685002.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorOrientation4153005117.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent156276569.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorButtonEvent4043921137.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager3364249716.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnG1804908545.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnA1967739812.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnT4143287487.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnOr602701282.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_OnBu358370788.h"
#include "AssemblyU2DCSharp_Gvr_Internal_EmulatorManager_U3C4253624923.h"
#include "AssemblyU2DCSharp_proto_Proto_PhoneEvent3882078222.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent2572128318.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types3648109718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Type1530480861.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve4072706903.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1262104803.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve1211758263.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve2701542133.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_MotionEve3452538341.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeE182225200.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_GyroscopeEv33558588.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1893725728.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Accelerom1480486140.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE1516604558.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_DepthMapE3483346914.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2038376807.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_Orientati2561526853.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent639576718.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Types_KeyEvent_2056133158.h"
#include "AssemblyU2DCSharp_proto_PhoneEvent_Builder2537253112.h"
#include "AssemblyU2DCSharp_GvrBasePointer2150122635.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster1189534163.h"
#include "AssemblyU2DCSharp_GvrBasePointerRaycaster_RaycastM3965091944.h"
#include "AssemblyU2DCSharp_GvrExecuteEventsExtension3083691626.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster1649506702.h"
#include "AssemblyU2DCSharp_GvrPointerGraphicRaycaster_Block4215129352.h"
#include "AssemblyU2DCSharp_GvrPointerInputModule1603976810.h"
#include "AssemblyU2DCSharp_GvrPointerPhysicsRaycaster2558158517.h"
#include "AssemblyU2DCSharp_GvrUnitySdkVersion4210256426.h"
#include "AssemblyU2DCSharp_GvrViewer2583885279.h"
#include "AssemblyU2DCSharp_GvrViewer_DistortionCorrectionMe1613770858.h"
#include "AssemblyU2DCSharp_GvrViewer_StereoScreenChangeDele1350813851.h"
#include "AssemblyU2DCSharp_GvrViewer_Eye1346324485.h"
#include "AssemblyU2DCSharp_GvrViewer_Distortion351632083.h"
#include "AssemblyU2DCSharp_Pose3D3872859958.h"
#include "AssemblyU2DCSharp_MutablePose3D1015643808.h"
#include "AssemblyU2DCSharp_GvrDropdown2234606196.h"
#include "AssemblyU2DCSharp_GvrLaserPointer2879974839.h"
#include "AssemblyU2DCSharp_GvrLaserPointerImpl2141976067.h"
#include "AssemblyU2DCSharp_GvrReticlePointer2836438988.h"
#include "AssemblyU2DCSharp_GvrReticlePointerImpl3911945438.h"
#include "AssemblyU2DCSharp_GvrActivityHelper1610839920.h"
#include "AssemblyU2DCSharp_GvrFPS750935016.h"
#include "AssemblyU2DCSharp_GvrIntent542233401.h"
#include "AssemblyU2DCSharp_GvrUIHelpers1244281516.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture673526704.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoType3515677472.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoResol1153223030.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoPlaye2474362314.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_VideoEvents660621533.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_RenderComma104445810.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnVideoEve3117232894.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_OnExceptio1653610982.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CStartU32919987970.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CCallPlu2924899295.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInternal129051828.h"
#include "AssemblyU2DCSharp_GvrVideoPlayerTexture_U3CInterna2021693473.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (GvrAudio_t2627885619), -1, sizeof(GvrAudio_t2627885619_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1900[23] = 
{
	GvrAudio_t2627885619_StaticFields::get_offset_of_sampleRate_0(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_numChannels_1(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_framesPerBuffer_2(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_listenerDirectivityColor_3(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_sourceDirectivityColor_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrAudio_t2627885619_StaticFields::get_offset_of_bounds_16(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_enabledRooms_17(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_initialized_18(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_listenerTransform_19(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_occlusionMaskValue_20(),
	GvrAudio_t2627885619_StaticFields::get_offset_of_pose_21(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (Quality_t2125366261)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[4] = 
{
	Quality_t2125366261::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (SpatializerData_t1929858338)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[9] = 
{
	SpatializerData_t1929858338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (SpatializerType_t3348390394)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	SpatializerType_t3348390394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (RoomProperties_t2834448096)+ sizeof (Il2CppObject), sizeof(RoomProperties_t2834448096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[20] = 
{
	RoomProperties_t2834448096::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_positionZ_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationX_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationY_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationZ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_rotationW_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsX_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsY_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_dimensionsZ_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialLeft_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialRight_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialBottom_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialTop_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialFront_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_materialBack_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reflectionScalar_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbGain_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbTime_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RoomProperties_t2834448096::get_offset_of_reverbBrightness_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (GvrAudioListener_t1521766837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[3] = 
{
	GvrAudioListener_t1521766837::get_offset_of_globalGainDb_2(),
	GvrAudioListener_t1521766837::get_offset_of_occlusionMask_3(),
	GvrAudioListener_t1521766837::get_offset_of_quality_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (GvrAudioRoom_t1253442178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[11] = 
{
	GvrAudioRoom_t1253442178::get_offset_of_leftWall_2(),
	GvrAudioRoom_t1253442178::get_offset_of_rightWall_3(),
	GvrAudioRoom_t1253442178::get_offset_of_floor_4(),
	GvrAudioRoom_t1253442178::get_offset_of_ceiling_5(),
	GvrAudioRoom_t1253442178::get_offset_of_backWall_6(),
	GvrAudioRoom_t1253442178::get_offset_of_frontWall_7(),
	GvrAudioRoom_t1253442178::get_offset_of_reflectivity_8(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbGainDb_9(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbBrightness_10(),
	GvrAudioRoom_t1253442178::get_offset_of_reverbTime_11(),
	GvrAudioRoom_t1253442178::get_offset_of_size_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (SurfaceMaterial_t3590751945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1907[24] = 
{
	SurfaceMaterial_t3590751945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (GvrAudioSoundfield_t1301118448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[13] = 
{
	GvrAudioSoundfield_t1301118448::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSoundfield_t1301118448::get_offset_of_gainDb_3(),
	GvrAudioSoundfield_t1301118448::get_offset_of_playOnAwake_4(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0102_5(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldClip0304_6(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldLoop_7(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldMute_8(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPitch_9(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldPriority_10(),
	GvrAudioSoundfield_t1301118448::get_offset_of_soundfieldVolume_11(),
	GvrAudioSoundfield_t1301118448::get_offset_of_id_12(),
	GvrAudioSoundfield_t1301118448::get_offset_of_audioSources_13(),
	GvrAudioSoundfield_t1301118448::get_offset_of_isPaused_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (GvrAudioSource_t2307460312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[25] = 
{
	GvrAudioSource_t2307460312::get_offset_of_bypassRoomEffects_2(),
	GvrAudioSource_t2307460312::get_offset_of_directivityAlpha_3(),
	GvrAudioSource_t2307460312::get_offset_of_directivitySharpness_4(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivityAlpha_5(),
	GvrAudioSource_t2307460312::get_offset_of_listenerDirectivitySharpness_6(),
	GvrAudioSource_t2307460312::get_offset_of_gainDb_7(),
	GvrAudioSource_t2307460312::get_offset_of_occlusionEnabled_8(),
	GvrAudioSource_t2307460312::get_offset_of_playOnAwake_9(),
	GvrAudioSource_t2307460312::get_offset_of_sourceClip_10(),
	GvrAudioSource_t2307460312::get_offset_of_sourceLoop_11(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMute_12(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePitch_13(),
	GvrAudioSource_t2307460312::get_offset_of_sourcePriority_14(),
	GvrAudioSource_t2307460312::get_offset_of_sourceDopplerLevel_15(),
	GvrAudioSource_t2307460312::get_offset_of_sourceSpread_16(),
	GvrAudioSource_t2307460312::get_offset_of_sourceVolume_17(),
	GvrAudioSource_t2307460312::get_offset_of_sourceRolloffMode_18(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMaxDistance_19(),
	GvrAudioSource_t2307460312::get_offset_of_sourceMinDistance_20(),
	GvrAudioSource_t2307460312::get_offset_of_hrtfEnabled_21(),
	GvrAudioSource_t2307460312::get_offset_of_audioSource_22(),
	GvrAudioSource_t2307460312::get_offset_of_id_23(),
	GvrAudioSource_t2307460312::get_offset_of_currentOcclusion_24(),
	GvrAudioSource_t2307460312::get_offset_of_nextOcclusionUpdate_25(),
	GvrAudioSource_t2307460312::get_offset_of_isPaused_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (GvrArmModel_t1664224602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (GvrArmModelOffsets_t2241056642), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (GvrController_t1602869021), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (GvrControllerVisual_t3328916665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (GvrControllerVisualManager_t1857939020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (GvrPointerManager_t2205699129), -1, sizeof(GvrPointerManager_t2205699129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1915[2] = 
{
	GvrPointerManager_t2205699129_StaticFields::get_offset_of_instance_2(),
	GvrPointerManager_t2205699129::get_offset_of_pointer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (GvrRecenterOnlyController_t2745329441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (GvrTooltip_t801170144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (AndroidNativeControllerProvider_t1389606029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (EmulatorClientSocket_t2001911543), -1, sizeof(EmulatorClientSocket_t2001911543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1919[9] = 
{
	EmulatorClientSocket_t2001911543_StaticFields::get_offset_of_kPhoneEventPort_2(),
	0,
	0,
	EmulatorClientSocket_t2001911543::get_offset_of_phoneMirroringSocket_5(),
	EmulatorClientSocket_t2001911543::get_offset_of_phoneEventThread_6(),
	EmulatorClientSocket_t2001911543::get_offset_of_shouldStop_7(),
	EmulatorClientSocket_t2001911543::get_offset_of_lastConnectionAttemptWasSuccessful_8(),
	EmulatorClientSocket_t2001911543::get_offset_of_phoneRemote_9(),
	EmulatorClientSocket_t2001911543::get_offset_of_U3CconnectedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (EmulatorConfig_t616150261), -1, sizeof(EmulatorConfig_t616150261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	EmulatorConfig_t616150261_StaticFields::get_offset_of_instance_2(),
	EmulatorConfig_t616150261::get_offset_of_PHONE_EVENT_MODE_3(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_USB_SERVER_IP_4(),
	EmulatorConfig_t616150261_StaticFields::get_offset_of_WIFI_SERVER_IP_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (Mode_t1624619217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1921[4] = 
{
	Mode_t1624619217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (EmulatorGyroEvent_t1858389926)+ sizeof (Il2CppObject), sizeof(EmulatorGyroEvent_t1858389926 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[2] = 
{
	EmulatorGyroEvent_t1858389926::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorGyroEvent_t1858389926::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (EmulatorAccelEvent_t621139879)+ sizeof (Il2CppObject), sizeof(EmulatorAccelEvent_t621139879 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[2] = 
{
	EmulatorAccelEvent_t621139879::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorAccelEvent_t621139879::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (EmulatorTouchEvent_t1122923020)+ sizeof (Il2CppObject), -1, sizeof(EmulatorTouchEvent_t1122923020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1924[6] = 
{
	EmulatorTouchEvent_t1122923020::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020::get_offset_of_relativeTimestamp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020::get_offset_of_pointers_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_POINTER_INDEX_SHIFT_3(),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_POINTER_INDEX_MASK_4(),
	EmulatorTouchEvent_t1122923020_StaticFields::get_offset_of_ACTION_MASK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (Action_t936529327)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[10] = 
{
	Action_t936529327::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Pointer_t3000685002)+ sizeof (Il2CppObject), sizeof(Pointer_t3000685002 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[3] = 
{
	Pointer_t3000685002::get_offset_of_fingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3000685002::get_offset_of_normalizedX_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Pointer_t3000685002::get_offset_of_normalizedY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (EmulatorOrientationEvent_t4153005117)+ sizeof (Il2CppObject), sizeof(EmulatorOrientationEvent_t4153005117 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1927[2] = 
{
	EmulatorOrientationEvent_t4153005117::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorOrientationEvent_t4153005117::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (EmulatorButtonEvent_t156276569)+ sizeof (Il2CppObject), sizeof(EmulatorButtonEvent_t156276569_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1928[2] = 
{
	EmulatorButtonEvent_t156276569::get_offset_of_code_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmulatorButtonEvent_t156276569::get_offset_of_down_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (ButtonCode_t4043921137)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[7] = 
{
	ButtonCode_t4043921137::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (EmulatorManager_t3364249716), -1, sizeof(EmulatorManager_t3364249716_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[16] = 
{
	EmulatorManager_t3364249716::get_offset_of_emulatorUpdate_2(),
	EmulatorManager_t3364249716::get_offset_of_waitForEndOfFrame_3(),
	EmulatorManager_t3364249716_StaticFields::get_offset_of_instance_4(),
	EmulatorManager_t3364249716::get_offset_of_currentGyroEvent_5(),
	EmulatorManager_t3364249716::get_offset_of_currentAccelEvent_6(),
	EmulatorManager_t3364249716::get_offset_of_currentTouchEvent_7(),
	EmulatorManager_t3364249716::get_offset_of_currentOrientationEvent_8(),
	EmulatorManager_t3364249716::get_offset_of_currentButtonEvent_9(),
	EmulatorManager_t3364249716::get_offset_of_gyroEventListenersInternal_10(),
	EmulatorManager_t3364249716::get_offset_of_accelEventListenersInternal_11(),
	EmulatorManager_t3364249716::get_offset_of_touchEventListenersInternal_12(),
	EmulatorManager_t3364249716::get_offset_of_orientationEventListenersInternal_13(),
	EmulatorManager_t3364249716::get_offset_of_buttonEventListenersInternal_14(),
	EmulatorManager_t3364249716::get_offset_of_pendingEvents_15(),
	EmulatorManager_t3364249716::get_offset_of_socket_16(),
	EmulatorManager_t3364249716::get_offset_of_lastDownTimeMs_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (OnGyroEvent_t1804908545), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (OnAccelEvent_t1967739812), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (OnTouchEvent_t4143287487), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (OnOrientationEvent_t602701282), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (OnButtonEvent_t358370788), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t4253624923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[5] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24locvar0_0(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24this_1(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24current_2(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24disposing_3(),
	U3CEndOfFrameU3Ec__Iterator0_t4253624923::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (PhoneEvent_t3882078222), -1, sizeof(PhoneEvent_t3882078222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	PhoneEvent_t3882078222_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (PhoneEvent_t2572128318), -1, sizeof(PhoneEvent_t2572128318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1938[25] = 
{
	PhoneEvent_t2572128318_StaticFields::get_offset_of_defaultInstance_0(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of__phoneEventFieldNames_1(),
	PhoneEvent_t2572128318_StaticFields::get_offset_of__phoneEventFieldTags_2(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasType_4(),
	PhoneEvent_t2572128318::get_offset_of_type__5(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasMotionEvent_7(),
	PhoneEvent_t2572128318::get_offset_of_motionEvent__8(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasGyroscopeEvent_10(),
	PhoneEvent_t2572128318::get_offset_of_gyroscopeEvent__11(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasAccelerometerEvent_13(),
	PhoneEvent_t2572128318::get_offset_of_accelerometerEvent__14(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasDepthMapEvent_16(),
	PhoneEvent_t2572128318::get_offset_of_depthMapEvent__17(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasOrientationEvent_19(),
	PhoneEvent_t2572128318::get_offset_of_orientationEvent__20(),
	0,
	PhoneEvent_t2572128318::get_offset_of_hasKeyEvent_22(),
	PhoneEvent_t2572128318::get_offset_of_keyEvent__23(),
	PhoneEvent_t2572128318::get_offset_of_memoizedSerializedSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (Types_t3648109718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (Type_t1530480861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1940[7] = 
{
	Type_t1530480861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (MotionEvent_t4072706903), -1, sizeof(MotionEvent_t4072706903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1941[12] = 
{
	MotionEvent_t4072706903_StaticFields::get_offset_of_defaultInstance_0(),
	MotionEvent_t4072706903_StaticFields::get_offset_of__motionEventFieldNames_1(),
	MotionEvent_t4072706903_StaticFields::get_offset_of__motionEventFieldTags_2(),
	0,
	MotionEvent_t4072706903::get_offset_of_hasTimestamp_4(),
	MotionEvent_t4072706903::get_offset_of_timestamp__5(),
	0,
	MotionEvent_t4072706903::get_offset_of_hasAction_7(),
	MotionEvent_t4072706903::get_offset_of_action__8(),
	0,
	MotionEvent_t4072706903::get_offset_of_pointers__10(),
	MotionEvent_t4072706903::get_offset_of_memoizedSerializedSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (Types_t1262104803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (Pointer_t1211758263), -1, sizeof(Pointer_t1211758263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1943[13] = 
{
	Pointer_t1211758263_StaticFields::get_offset_of_defaultInstance_0(),
	Pointer_t1211758263_StaticFields::get_offset_of__pointerFieldNames_1(),
	Pointer_t1211758263_StaticFields::get_offset_of__pointerFieldTags_2(),
	0,
	Pointer_t1211758263::get_offset_of_hasId_4(),
	Pointer_t1211758263::get_offset_of_id__5(),
	0,
	Pointer_t1211758263::get_offset_of_hasNormalizedX_7(),
	Pointer_t1211758263::get_offset_of_normalizedX__8(),
	0,
	Pointer_t1211758263::get_offset_of_hasNormalizedY_10(),
	Pointer_t1211758263::get_offset_of_normalizedY__11(),
	Pointer_t1211758263::get_offset_of_memoizedSerializedSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (Builder_t2701542133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[2] = 
{
	Builder_t2701542133::get_offset_of_resultIsReadOnly_0(),
	Builder_t2701542133::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (Builder_t3452538341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[2] = 
{
	Builder_t3452538341::get_offset_of_resultIsReadOnly_0(),
	Builder_t3452538341::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (GyroscopeEvent_t182225200), -1, sizeof(GyroscopeEvent_t182225200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[16] = 
{
	GyroscopeEvent_t182225200_StaticFields::get_offset_of_defaultInstance_0(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of__gyroscopeEventFieldNames_1(),
	GyroscopeEvent_t182225200_StaticFields::get_offset_of__gyroscopeEventFieldTags_2(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasTimestamp_4(),
	GyroscopeEvent_t182225200::get_offset_of_timestamp__5(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasX_7(),
	GyroscopeEvent_t182225200::get_offset_of_x__8(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasY_10(),
	GyroscopeEvent_t182225200::get_offset_of_y__11(),
	0,
	GyroscopeEvent_t182225200::get_offset_of_hasZ_13(),
	GyroscopeEvent_t182225200::get_offset_of_z__14(),
	GyroscopeEvent_t182225200::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (Builder_t33558588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[2] = 
{
	Builder_t33558588::get_offset_of_resultIsReadOnly_0(),
	Builder_t33558588::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (AccelerometerEvent_t1893725728), -1, sizeof(AccelerometerEvent_t1893725728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1948[16] = 
{
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of_defaultInstance_0(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of__accelerometerEventFieldNames_1(),
	AccelerometerEvent_t1893725728_StaticFields::get_offset_of__accelerometerEventFieldTags_2(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasTimestamp_4(),
	AccelerometerEvent_t1893725728::get_offset_of_timestamp__5(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasX_7(),
	AccelerometerEvent_t1893725728::get_offset_of_x__8(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasY_10(),
	AccelerometerEvent_t1893725728::get_offset_of_y__11(),
	0,
	AccelerometerEvent_t1893725728::get_offset_of_hasZ_13(),
	AccelerometerEvent_t1893725728::get_offset_of_z__14(),
	AccelerometerEvent_t1893725728::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Builder_t1480486140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[2] = 
{
	Builder_t1480486140::get_offset_of_resultIsReadOnly_0(),
	Builder_t1480486140::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (DepthMapEvent_t1516604558), -1, sizeof(DepthMapEvent_t1516604558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1950[16] = 
{
	DepthMapEvent_t1516604558_StaticFields::get_offset_of_defaultInstance_0(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of__depthMapEventFieldNames_1(),
	DepthMapEvent_t1516604558_StaticFields::get_offset_of__depthMapEventFieldTags_2(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasTimestamp_4(),
	DepthMapEvent_t1516604558::get_offset_of_timestamp__5(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasWidth_7(),
	DepthMapEvent_t1516604558::get_offset_of_width__8(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_hasHeight_10(),
	DepthMapEvent_t1516604558::get_offset_of_height__11(),
	0,
	DepthMapEvent_t1516604558::get_offset_of_zDistancesMemoizedSerializedSize_13(),
	DepthMapEvent_t1516604558::get_offset_of_zDistances__14(),
	DepthMapEvent_t1516604558::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (Builder_t3483346914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	Builder_t3483346914::get_offset_of_resultIsReadOnly_0(),
	Builder_t3483346914::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (OrientationEvent_t2038376807), -1, sizeof(OrientationEvent_t2038376807_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1952[19] = 
{
	OrientationEvent_t2038376807_StaticFields::get_offset_of_defaultInstance_0(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of__orientationEventFieldNames_1(),
	OrientationEvent_t2038376807_StaticFields::get_offset_of__orientationEventFieldTags_2(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasTimestamp_4(),
	OrientationEvent_t2038376807::get_offset_of_timestamp__5(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasX_7(),
	OrientationEvent_t2038376807::get_offset_of_x__8(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasY_10(),
	OrientationEvent_t2038376807::get_offset_of_y__11(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasZ_13(),
	OrientationEvent_t2038376807::get_offset_of_z__14(),
	0,
	OrientationEvent_t2038376807::get_offset_of_hasW_16(),
	OrientationEvent_t2038376807::get_offset_of_w__17(),
	OrientationEvent_t2038376807::get_offset_of_memoizedSerializedSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (Builder_t2561526853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[2] = 
{
	Builder_t2561526853::get_offset_of_resultIsReadOnly_0(),
	Builder_t2561526853::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (KeyEvent_t639576718), -1, sizeof(KeyEvent_t639576718_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1954[10] = 
{
	KeyEvent_t639576718_StaticFields::get_offset_of_defaultInstance_0(),
	KeyEvent_t639576718_StaticFields::get_offset_of__keyEventFieldNames_1(),
	KeyEvent_t639576718_StaticFields::get_offset_of__keyEventFieldTags_2(),
	0,
	KeyEvent_t639576718::get_offset_of_hasAction_4(),
	KeyEvent_t639576718::get_offset_of_action__5(),
	0,
	KeyEvent_t639576718::get_offset_of_hasCode_7(),
	KeyEvent_t639576718::get_offset_of_code__8(),
	KeyEvent_t639576718::get_offset_of_memoizedSerializedSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (Builder_t2056133158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[2] = 
{
	Builder_t2056133158::get_offset_of_resultIsReadOnly_0(),
	Builder_t2056133158::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (Builder_t2537253112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[2] = 
{
	Builder_t2537253112::get_offset_of_resultIsReadOnly_0(),
	Builder_t2537253112::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (GvrBasePointer_t2150122635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[2] = 
{
	GvrBasePointer_t2150122635::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_0(),
	GvrBasePointer_t2150122635::get_offset_of_U3CPointerTransformU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (GvrBasePointerRaycaster_t1189534163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[2] = 
{
	GvrBasePointerRaycaster_t1189534163::get_offset_of_raycastMode_2(),
	GvrBasePointerRaycaster_t1189534163::get_offset_of_lastRay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (RaycastMode_t3965091944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[3] = 
{
	RaycastMode_t3965091944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (GvrExecuteEventsExtension_t3083691626), -1, sizeof(GvrExecuteEventsExtension_t3083691626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	GvrExecuteEventsExtension_t3083691626_StaticFields::get_offset_of_s_HoverHandler_0(),
	GvrExecuteEventsExtension_t3083691626_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (GvrPointerGraphicRaycaster_t1649506702), -1, sizeof(GvrPointerGraphicRaycaster_t1649506702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1963[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_ignoreReversedGraphics_5(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingObjects_6(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_blockingMask_7(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_targetCanvas_8(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_raycastResults_9(),
	GvrPointerGraphicRaycaster_t1649506702::get_offset_of_cachedPointerEventCamera_10(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_sortedGraphics_11(),
	GvrPointerGraphicRaycaster_t1649506702_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (BlockingObjects_t4215129352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[5] = 
{
	BlockingObjects_t4215129352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (GvrPointerInputModule_t1603976810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[9] = 
{
	GvrPointerInputModule_t1603976810::get_offset_of_vrModeOnly_8(),
	GvrPointerInputModule_t1603976810::get_offset_of_pointerData_9(),
	GvrPointerInputModule_t1603976810::get_offset_of_lastPose_10(),
	GvrPointerInputModule_t1603976810::get_offset_of_lastScroll_11(),
	GvrPointerInputModule_t1603976810::get_offset_of_eligibleForScroll_12(),
	GvrPointerInputModule_t1603976810::get_offset_of_isPointerHovering_13(),
	GvrPointerInputModule_t1603976810::get_offset_of_isActive_14(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (GvrPointerPhysicsRaycaster_t2558158517), -1, sizeof(GvrPointerPhysicsRaycaster_t2558158517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1966[4] = 
{
	0,
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_raycasterEventMask_5(),
	GvrPointerPhysicsRaycaster_t2558158517::get_offset_of_cachedEventCamera_6(),
	GvrPointerPhysicsRaycaster_t2558158517_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (GvrUnitySdkVersion_t4210256426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (GvrViewer_t2583885279), -1, sizeof(GvrViewer_t2583885279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1968[20] = 
{
	0,
	GvrViewer_t2583885279_StaticFields::get_offset_of_instance_3(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_currentController_4(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_currentMainCamera_5(),
	GvrViewer_t2583885279::get_offset_of_vrModeEnabled_6(),
	GvrViewer_t2583885279::get_offset_of_distortionCorrection_7(),
	GvrViewer_t2583885279::get_offset_of_neckModelScale_8(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_device_9(),
	GvrViewer_t2583885279::get_offset_of_U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_10(),
	GvrViewer_t2583885279::get_offset_of_U3CNativeUILayerSupportedU3Ek__BackingField_11(),
	GvrViewer_t2583885279::get_offset_of_stereoScreenScale_12(),
	GvrViewer_t2583885279_StaticFields::get_offset_of_stereoScreen_13(),
	GvrViewer_t2583885279::get_offset_of_OnStereoScreenChanged_14(),
	GvrViewer_t2583885279::get_offset_of_defaultComfortableViewingRange_15(),
	GvrViewer_t2583885279::get_offset_of_DefaultDeviceProfile_16(),
	GvrViewer_t2583885279::get_offset_of_U3CTriggeredU3Ek__BackingField_17(),
	GvrViewer_t2583885279::get_offset_of_U3CTiltedU3Ek__BackingField_18(),
	GvrViewer_t2583885279::get_offset_of_U3CProfileChangedU3Ek__BackingField_19(),
	GvrViewer_t2583885279::get_offset_of_U3CBackButtonPressedU3Ek__BackingField_20(),
	GvrViewer_t2583885279::get_offset_of_updatedToFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (DistortionCorrectionMethod_t1613770858)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[4] = 
{
	DistortionCorrectionMethod_t1613770858::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (StereoScreenChangeDelegate_t1350813851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (Eye_t1346324485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	Eye_t1346324485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (Distortion_t351632083)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[3] = 
{
	Distortion_t351632083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (Pose3D_t3872859958), -1, sizeof(Pose3D_t3872859958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1973[4] = 
{
	Pose3D_t3872859958_StaticFields::get_offset_of_flipZ_0(),
	Pose3D_t3872859958::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t3872859958::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t3872859958::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (MutablePose3D_t1015643808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (GvrDropdown_t2234606196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[1] = 
{
	GvrDropdown_t2234606196::get_offset_of_currentBlocker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (GvrLaserPointer_t2879974839), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (GvrLaserPointerImpl_t2141976067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[11] = 
{
	0,
	0,
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMainCameraU3Ek__BackingField_4(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CLaserColorU3Ek__BackingField_5(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CLaserLineRendererU3Ek__BackingField_6(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CReticleU3Ek__BackingField_7(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMaxLaserDistanceU3Ek__BackingField_8(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CMaxReticleDistanceU3Ek__BackingField_9(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CPointerIntersectionU3Ek__BackingField_10(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CIsPointerIntersectingU3Ek__BackingField_11(),
	GvrLaserPointerImpl_t2141976067::get_offset_of_U3CPointerIntersectionRayU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (GvrReticlePointer_t2836438988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[3] = 
{
	GvrReticlePointer_t2836438988::get_offset_of_reticlePointerImpl_2(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleSegments_3(),
	GvrReticlePointer_t2836438988::get_offset_of_reticleGrowthSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (GvrReticlePointerImpl_t3911945438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[12] = 
{
	0,
	0,
	0,
	0,
	0,
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleGrowthSpeedU3Ek__BackingField_7(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CMaterialCompU3Ek__BackingField_8(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_9(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_10(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_11(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_12(),
	GvrReticlePointerImpl_t3911945438::get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (GvrActivityHelper_t1610839920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (GvrFPS_t750935016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[6] = 
{
	0,
	0,
	0,
	GvrFPS_t750935016::get_offset_of_textField_5(),
	GvrFPS_t750935016::get_offset_of_fps_6(),
	GvrFPS_t750935016::get_offset_of_cam_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (GvrIntent_t542233401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (GvrUIHelpers_t1244281516), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (GvrVideoPlayerTexture_t673526704), -1, sizeof(GvrVideoPlayerTexture_t673526704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1984[33] = 
{
	0,
	0,
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoTextures_4(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_currentTexture_5(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoPlayerPtr_6(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoPlayerEventBase_7(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialTexture_8(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialized_9(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_texWidth_10(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_texHeight_11(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_lastBufferedPosition_12(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_framecount_13(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_graphicComponent_14(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_rendererComponent_15(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_renderEventFunction_16(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_processingRunning_17(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_exitProcessing_18(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_onEventCallbacks_19(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_onExceptionCallbacks_20(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_ExecuteOnMainThread_21(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_statusText_22(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_bufferSize_23(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_t673526704::get_offset_of_useSecurePath_30(),
	0,
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	GvrVideoPlayerTexture_t673526704_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (VideoType_t3515677472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[4] = 
{
	VideoType_t3515677472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (VideoResolution_t1153223030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[6] = 
{
	VideoResolution_t1153223030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (VideoPlayerState_t2474362314)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[6] = 
{
	VideoPlayerState_t2474362314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (VideoEvents_t660621533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[6] = 
{
	VideoEvents_t660621533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (RenderCommand_t104445810)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[8] = 
{
	RenderCommand_t104445810::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (OnVideoEventCallback_t3117232894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (OnExceptionCallback_t1653610982), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (U3CStartU3Ec__Iterator0_t2919987970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[4] = 
{
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2919987970::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[10] = 
{
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CrunningU3E__0_0(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CwfeofU3E__0_1(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CtexU3E__1_2(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CwU3E__1_3(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3ChU3E__1_4(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U3CbpU3E__1_5(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24this_6(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24current_7(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24disposing_8(),
	U3CCallPluginAtEndOfFramesU3Ec__Iterator1_t2924899295::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_player_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey2_t129051828::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_player_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_type_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey3_t2021693473::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1996[6] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
